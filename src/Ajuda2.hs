module Ajuda2 where
import System.IO
import Data.Char (isDigit)
import System.Environment
import Data.Char


addEnd :: [String] -> String -> [String]
addEnd [] n = [n]
addEnd [x] n = [x,n]
addEnd (x:xs) n = x : (addEnd xs n)


stringToInt :: String -> Int
stringToInt [] = 0
stringToInt a = read (((takeWhile (/='.') (a)))) :: Int

posicoes1Coluna :: [String] -> Int -> Int
posicoes1Coluna [] _ = 0
posicoes1Coluna (x:xs) jog = if (posicoesAux x jog) then read ((words x) !! 1) :: Int
                                       else posicoes1Coluna xs jog

posicoes1Linha :: [String] -> Int -> Int
posicoes1Linha [] _ = 0
posicoes1Linha (x:xs) jog = if (posicoesAux x jog) then read ((words x) !! 2) :: Int
                                      else posicoes1Linha xs jog


posicoesAux :: String -> Int -> Bool
posicoesAux (x:xs) jog | x == (intToDigit jog) = True
                       | otherwise = False












--  moveVAl diz se um movimento é valido ou nao
moveVal :: [String] -> Char -> (Int,Int) -> Bool
moveVal _ _ (0,_) = False
moveVal l k (col,lin)| k == 'U' && ((l !! (lin - 1)) !! (col)) /= '#' && ((l !! (lin - 1)) !! (col)) /= '?' = True
        | k == 'D' && ((l !! (lin + 1)) !! (col)) /= '#' && ((l !! (lin + 1)) !! (col)) /= '?' = True
        | k == 'L' && ((l !! (lin)) !! (col - 1)) /= '#' && ((l !! (lin)) !! (col - 1)) /= '?' = True
        | k == 'R' && ((l !! (lin)) !! (col + 1)) /= '#' && ((l !! (lin)) !! (col + 1)) /= '?' = True
        | otherwise = False





-- a comando: se o movimento for valido vai dar a nova posiçao a seguir a dar UP ou Down ou Left ou RIght
comando :: [String] -> Char -> Int -> Int -> Int -> String -> [String]
comando [] _ _ _ _ _ = []
comando l key jog col lin []  | key == 'U' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show col ++ " " ++ show (lin - 1)))
                 | key == 'D' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show col ++ " " ++ show (lin + 1)))
                 | key == 'L' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show (col - 1) ++ " " ++ show lin))
                 | key == 'R' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show (col + 1) ++ " " ++ show lin))
                 | otherwise = (addEnd l (intToDigit (jog) : ' ' : show (col) ++ " " ++ show lin))

comando l key jog col lin ppw | key == 'U' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show col ++ " " ++ show (lin - 1)++" "++ppw))
                 | key == 'D' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show col ++ " " ++ show (lin + 1)++" "++ppw))
                 | key == 'L' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show (col - 1) ++ " " ++ show lin++" "++ppw))
                 | key == 'R' && (moveVal l key (col,lin)) = (addEnd l (intToDigit (jog) : ' ' : show (col + 1) ++ " " ++ show lin++" "++ppw))
                 | otherwise = (addEnd l (intToDigit (jog) : ' ' : show (col) ++ " " ++ show lin++" "++ppw))

























powerUpAux :: [String] -> Int -> Int -> Int -> [String]
powerUpAux (x:xs) jog col lin | head x == intToDigit jog = (x ++ "+"):xs
                 | otherwise = x : powerUpAux xs jog col lin

powerUpAux1 :: [String] -> Int -> Int -> Int -> [String]
powerUpAux1 (x:xs) jog col lin | head x == intToDigit jog = (x ++ "!") :xs
                  | otherwise = x : powerUpAux1 xs jog col lin

powerUp1 :: [String] -> Int -> Int -> Int -> [String]
powerUp1 [] _ _ _ = []
powerUp1 (x:xs) jog col lin | (remover2BombsAux x col lin) && (head x) == '+' = (powerUpAux (x:xs) jog col lin)
               | (remover2FlamsAux x col lin) && (head x) == '!' = (powerUpAux1 (x:xs) jog col lin)
               | otherwise = x : powerUp1 xs jog col lin




--remove as coordenadas
remover2Bombs :: [String] -> Int -> Int -> [String]
remover2Bombs [] _ _ = []
remover2Bombs (x:xs) col lin = if remover2BombsAux x col lin then xs else x : remover2Bombs xs col lin

remover2BombsAux :: String -> Int -> Int -> Bool
remover2BombsAux (x:xs) col lin | (x == '+') && ((head (drop 1 xs)):[]) == show col && ((head (drop 3 xs)):[]) == show lin = True
                   | otherwise = False

remover2Flams :: [String] -> Int -> Int -> [String]
remover2Flams [] _ _ = []
remover2Flams (x:xs) col lin = if remover2FlamsAux x col lin then xs else x : remover2Flams xs col lin

remover2FlamsAux :: String -> Int -> Int -> Bool
remover2FlamsAux (x:xs) col lin | (x == '!') && ((head (drop 1 xs)):[]) == show col && ((head (drop 3 xs)):[]) == show lin = True
                   | otherwise = False








ex =  ["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","! 5 5","* 7 7 1 1 10","0 4 3 +","1 6 6"]
ex1 = ["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?     #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","* 7 7 1 1 10","0 5 4","1 7 6 +!!","2 5 4","3 1 6"]


move :: [String] -> Int -> Char -> [String]
move ex i c | c == 'B' = (ordena (bomba1 ex ex i)) --se o pedido for bomba, usa uma funçao para bombas
            | playerExiste ex i = ordppw (move5 ex i c) --senao usa funçao para movimento
            | otherwise = ex

playerExiste :: [String] -> Int -> Bool
playerExiste [] _ = False
playerExiste (h:t) i = if head h == intToDigit i
                       then True
                       else playerExiste t i

ordppw :: [String] -> [String]
ordppw [] = []
ordppw (h:t) = if head h == '0' || head h == '1' || head h == '2' || head h == '3'
  then (funcao h) : ordppw t
  else         h  : ordppw t

funcao :: String -> String
funcao [] = []
funcao (h:t) | h == '+'  = h:funcao t
             | h == '!'  = funcao t ++ "!"
             | otherwise = h:funcao t

move1 :: [String] -> Int -> Char -> [String]
move1 ex i c = if ocorre2 (comando1 ex i c) i
 then retira (comando1 ex i c) i
 else comando1 ex i c


move2 :: [String] -> Int -> Char -> [String]
move2 ex i c = movej3 (movej2 (movej1 (move1 ex i c) i c))


-- adiciona o powerup ao final das coordenadas do jogador
move3 :: [String] -> Int -> Char -> [String]
move3 ex i c  |c == 'U' = powerUp1 (move2 ex i c) i (posicoes1Coluna ex i) ((posicoes1Linha ex i) + 1)
              |c == 'D' = powerUp1 (move2 ex i c) i (posicoes1Coluna ex i) ((posicoes1Linha ex i) - 1)
              |c == 'L' = powerUp1 (move2 ex i c) i ((posicoes1Coluna ex i) - 1) (posicoes1Linha ex i)
              |c == 'R' = powerUp1 (move2 ex i c) i ((posicoes1Coluna ex i) + 1) (posicoes1Linha ex i)


--remove as coordenadas do powerup se for Bombs
move4 :: [String] -> Int -> Char -> [String]
move4 ex i c |c == 'U' = remover2Bombs (move3 ex i c) (posicoes1Coluna ex i) ((posicoes1Linha ex i) + 1)
             |c == 'D' = remover2Bombs (move3 ex i c) (posicoes1Coluna ex i) ((posicoes1Linha ex i) - 1)
             |c == 'L' = remover2Bombs (move3 ex i c) ((posicoes1Coluna ex i) - 1) (posicoes1Linha ex i)
             |c == 'R' = remover2Bombs (move3 ex i c) ((posicoes1Coluna ex i) + 1) (posicoes1Linha ex i)

-- remove as coordenadas do powerup se for flames
move5 :: [String] -> Int -> Char -> [String]
move5 ex i c |c == 'U' = remover2Flams (move4 ex i c) (posicoes1Coluna ex i) ((posicoes1Linha ex i) + 1)
             |c == 'D' = remover2Flams (move4 ex i c) (posicoes1Coluna ex i) ((posicoes1Linha ex i) - 1)
             |c == 'L' = remover2Flams (move4 ex i c) ((posicoes1Coluna ex i) - 1) (posicoes1Linha ex i)
             |c == 'R' = remover2Flams (move4 ex i c) ((posicoes1Coluna ex i) + 1) (posicoes1Linha ex i)

















ordena :: [String] -> [String]
ordena [x] = [x]
ordena (a:b:t) | head a == '*' && head b == '*' && ((!!) (words a) 2) <  ((!!) (words b) 2) = a : (ordena  (b:t))
  | head a == '*' && head b == '*' && ((!!) (words a) 2) >  ((!!) (words b) 2) = b : (ordena  (a:t))
  | head a == '*' && head b == '*' && ((!!) (words a) 2) == ((!!) (words b) 2) = (ordena1 (a:b:t))
  | otherwise = a : ordena (b:t)

ordena1 :: [String] -> [String]
ordena1 [x] = [x]
ordena1 (a:b:t) | head a == '*' && head b == '*' && ((!!) (words a) 1) <  ((!!) (words b) 1) = a : (ordena  (b:t))
   | head a == '*' && head b == '*' && ((!!) (words a) 1) >  ((!!) (words b) 1) = b : (ordena  (a:t))
   | otherwise = a : ordena1 (b:t)



movej1 :: [String] -> Int -> Char -> [String]
movej1 [] i c = []
movej1 ((x:xs):t) i c | x == '1'  = t ++ [x:xs]
         | otherwise = (x:xs) : movej1 t i c

movej2 :: [String] -> [String]
movej2 [] = []
movej2 ((x:xs):t) | x == '2'  = t ++ [x:xs]
     | otherwise = (x:xs) : movej2 t

movej3 :: [String] -> [String]
movej3 [] = []
movej3 ((x:xs):t) | x == '3'  = t ++ [x:xs]
     | otherwise = (x:xs) : movej3 t








comando1 :: [String] -> Int -> Char -> [String]
comando1 ex i c = comando ex c i (posicoes1Coluna ex i) (posicoes1Linha ex i) (powerups ex i)








powerups :: [String] -> Int -> String
powerups [] i =""
powerups ((x:xs):t) i = if x == head (show i)
           then powerups1 xs
           else powerups t i

powerups1 :: String -> String
powerups1 [] = []
powerups1 (h:t) = if h == '+' || h == '!'
     then (h:t)
     else powerups1 t



ocorre2 :: [String] -> Int -> Bool
ocorre2 [] i = False
ocorre2 [x] i = False
ocorre2 ((x:xs):(y:ys):t) i | x == y && y == head (show i) = True
               | x /= y && y == head (show i) = ocorre2 ((y:ys):t) i
               | x /= y && x == head (show i) = ocorre2 ((x:xs):t) i
               | otherwise = ocorre2 ((y:ys):t) i

retira :: [String] -> Int -> [String]
retira [] i = []
retira ((x:xs):t) i = if x == head (show i)
         then t
         else (x:xs):(retira t i)




{-bomba :: [String] -> [String] -> Int -> Int -> [String]
bomba [] l jog c = bomba1 l l jog
bomba (x:t) l jog c | head x == '*' && (jog' x 0 == jog) = if ((jogDaBomba (x:t) jog c) <  numBombasPoss jog l) then bomba1 l l jog else bomba t l jog c
       | otherwise = bomba t l jog c
-}

bomba1 :: [String] -> [String] -> Int -> [String]
bomba1 [] l i = l
bomba1 (h:t) l i = if head h == (intToDigit i) && jogaPode (intToDigit i) h l--procura a posiçao do jogador
   then bomba' (posicaoJog (h++"         ") 0) (intToDigit i) l h--guarda a posiçao do jogador powers e o mapa
   else bomba1 t l i --recursividade

bomba' :: String -> Char -> [String] -> String -> [String]
--z e do tipo " 1 1"
bomba' z i l h = if semBombas z l --verifica se o sitio esta vazio
    then colocaBomba ((reverse (i:' ':(reverse z)))++" "++(show (soMais h))++" 10") l
    else l


posicaoJog :: String -> Int -> String
posicaoJog (a:b:c:d:e:f:g:h:i:t) z | d == ' ' && f == ' ' = (b:c:d:e:[])
                      | e == ' ' && h == ' ' = (b:c:d:e:f:g:[])
                      | e == ' ' && g == ' ' = (b:c:d:e:f:[])
                      | d == ' ' && g == ' ' = (b:c:d:e:f:[])
                      | otherwise = if (a == ' ') then posicaoJog (b:c:d:e:f:g:h:i:t) (z+1) else posicaoJog (b:c:d:e:f:g:h:i:t) z


jogaPode :: Char -> String -> [String] -> Bool
jogaPode a (x:xs) (y:ys) | numberPU (x:xs) >= numberBombs a (y:ys) = True
            | otherwise = False



numberPU :: String -> Int
numberPU [] = 0
numberPU (h:t) = if h == '+'
    then 1 + numberPU t
    else numberPU t

numberBombs :: Char -> [String] -> Int
numberBombs a [] = 0
numberBombs a (h:t) = if head h == '*'
         then valorH h a 0 + numberBombs a t
         else numberBombs a t

valorH :: String -> Char -> Int -> Int
valorH [] j d = 0
valorH (h:t) j d | d == 3 && h == j = 1
    | otherwise = if h == ' ' then valorH t j (d+1) else valorH t j d

soMais :: String -> Int
soMais [] = 1
soMais (h:t) = if h == '!'
  then 1 + soMais t
  else soMais t

-- z = " 23 23"
semBombas :: String -> [String] -> Bool
semBombas _ [] = True
semBombas z (x:xs) | ('*':z) == take (length ('*':z)) x = False --compara se existe alguma bomba na posiçao onde o jogador que colocar uma
      | otherwise = semBombas z xs

colocaBomba :: String -> [String] -> [String]
colocaBomba z l = coloca ('*':z) (separa l) (separa1 l)

separa :: [String] -> [String]
separa [] = []
separa (h:t) | (head h) /= '0' && (head h) /= '1' && (head h) /= '2' && (head h) /= '3' = h:(separa t)
             | otherwise = separa t

separa1 :: [String] -> [String]
separa1 (h:t) = drop (length (separa (h:t))) (h:t)

coloca :: String -> [String] -> [String] -> [String]
coloca z l y = (reverse(z:(reverse l)))++y
