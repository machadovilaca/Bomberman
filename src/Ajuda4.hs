module Ajuda4 where
import Data.Char (isDigit)
import System.Environment
import Text.Read
import Data.Maybe
import Data.Char
import Data.List

type Map = [String]
type Bomba = String
type Coluna = Int
type Linha = Int
type Coordenadas = (Int,Int)
type Raio = Int
type Tempo = Int

avanca :: [String] -> Int -> [String]
avanca mapa time | time <= form  = substitui (ticks (bombasUltimate mapa) time) coord
                 | otherwise     =            ticks (bombasUltimate mapa) time

             where
                   coord = head (drop n (ultimateEspiral mapa time 1))
                   n     = form - time
                   form  = ((length (head mapa)-2)^2)

ex =  ["#########","#?      #","# #?#?# #","#? ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??  ?#","#########","+ 5 2","+ 3 6","! 4 5","* 3 5 0 2 1","* 2 1 0 1 1","0 4 3 +","1 7 6"]

--Esta funçao diminui o tempo das bombas em um, tem que ser chamada sempre que o clock diminuir um click
ticks :: Map -> Tempo -> [String]
ticks [] t = []
ticks (x:xs) tic = case head x of
                     '*' -> (mudarTempo2 x ((tempo x) - 1)) : ticks xs tic
                     _   -> x : ticks xs tic

------------------------------------------------------Funcoes principais das explosoes-----------------------------------------------------
------------Esta é a funcao principal que controla todas as explosoes
bombasUltimate :: Map -> Map
bombasUltimate mapa = whereisBombas mapa (stringsExp mapa (indiceBombas mapa 0))

--------Esta funçao da uma lista com as strings das bombas que tem de explodir
stringsExp :: Map -> [Int] -> [Bomba]
stringsExp mapa [] = []
stringsExp mapa (x:xs) = (mapa !! x) : stringsExp mapa xs

------------Esta funcao vai buscar o indice das linhas que têm bombas para explodir e poe nos numa lista, que é aplicada na funçao acima
indiceBombas :: Map -> Int -> [Int]
indiceBombas [] _ = []
indiceBombas (x:xs) i = if head x =='*' && tempo x == 1 then i : indiceBombas xs (i+1) else indiceBombas xs (i+1)



------------ Esta funcao recebe o mapa , a lista das bombas para explodir e vai explodir as strings
whereisBombas :: Map -> [Bomba] -> Map
whereisBombas (x:xs) bombas = case bombas of
                              []    -> (x:xs)
                              (h:t) -> whereisBombas (explodeALL (x:xs) h (colunaP h) (linhaP h) (raioB h)) t


---------Esta funcao invoca todas as explosoes (cima, baixo , esquerda, direita)
explodeALL :: Map -> Bomba -> Coluna -> Linha -> Raio -> Map
explodeALL [] _ _ _ _ = []
explodeALL mapa bomba c l r = case r of
                                0 -> mapa
                                _ -> (remBomba (raioZero (explodirR (explodirL (explodirD (explodirU mapa c l r) c l r) c l r) c l r) c l) c l)


--------------------------------------------------------------------------------------------------------------------------------------------
------------FUNCOES AUXILIARES DA explodeALL , sao as que destroem o que está em cima , baixo, esquerda e direita
explodirU :: Map -> Coluna -> Linha -> Raio -> Map
explodirU [] _ _ _ = []
explodirU mapa c l r    | r == 0 =  mapa
                        | l-1 <= 0 = mapa
                        | c <= 0 = mapa
                        | l >= length (head mapa) = mapa
                        | brick mapa c (l-1) = remover mapa c (l-1)
                        | rock mapa c (l-1) = mapa
                        | bomb mapa c (l-1) = (mudarTempo (explodirU mapa c (l-1) (r-1)) c (l-1))
                        | (powerUp mapa c (l-1)) && (player mapa c (l-1)) = removerStringP1 (removerStringP (explodirU mapa c (l-1) (r-1)) c (l-1)) c (l-1)
                        | player mapa c (l-1)  = removerStringP1 (explodirU mapa c (l-1) (r-1)) c (l-1)
                        | powerUp mapa c (l-1)  = (removerStringP (explodirU mapa c (l-1) (r-1)) c (l-1))
                        | otherwise = explodirU mapa c (l-1) (r-1)

explodirD :: Map -> Coluna -> Linha -> Raio -> Map
explodirD [] _ _ _ = []
explodirD mapa c l r    | r == 0 = mapa
                        | l+1 >= (length (head mapa)) = mapa
                        | c <= 0 = mapa
                        | l <= 0 = mapa
                        | brick mapa c (l+1) = remover mapa c (l+1)
                        | rock mapa c (l+1) = mapa
                        | bomb mapa c (l+1) = mudarTempo (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | (powerUp mapa c (l+1)) && (player mapa c (l+1)) = removerStringP1 (removerStringP (explodirD mapa c (l+1) (r-1)) c (l+1)) c (l+1)
                        | player mapa c (l+1) = removerStringP1 (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | powerUp mapa c (l+1)  = removerStringP (explodirD mapa c (l+1) (r-1)) c (l+1)
                        | otherwise = explodirD mapa c (l+1) (r-1)

explodirL :: Map -> Coluna -> Linha -> Raio -> Map
explodirL [] _ _ _ = []
explodirL mapa c l r    | r == 0 = mapa
                        | c <= 0 = mapa
                        | l >= length (head mapa) = mapa
                        | (c+1) >= length (head mapa) = mapa
                        | brick mapa (c-1) l = remover mapa (c-1) l
                        | rock mapa (c-1) l = mapa
                        | bomb mapa (c-1) l = mudarTempo ( explodirL mapa (c-1) l (r-1)) (c-1) l
                        | (powerUp mapa (c-1) l) && (player mapa (c-1) l) = removerStringP1 (removerStringP (explodirL mapa (c-1) l (r-1)) (c-1) l) (c-1) l
                        | player mapa (c-1) l  =  removerStringP1 (explodirL mapa (c-1) l (r-1)) (c-1) l
                        | powerUp mapa (c-1) l  = removerStringP (explodirL mapa (c-1) l (r-1)) (c-1) l
                        | otherwise = explodirL mapa (c-1) l (r-1)

explodirR :: Map -> Coluna -> Linha -> Raio -> Map
explodirR [] _ _ _ = []
explodirR mapa c l r    | r == 0 =  mapa
                        | c+1 >= (length (head mapa)) = mapa
                        | l >= length (head mapa) = mapa
                        | brick mapa (c+1) l = remover mapa (c+1) l
                        | rock mapa (c+1) l = mapa
                        | bomb mapa (c+1) l = mudarTempo (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | (powerUp mapa (c+1) l) && (player mapa (c+1) l) = removerStringP1 (removerStringP (explodirR mapa (c+1) l (r-1)) (c+1) l) (c+1) l
                        | player mapa (c+1) l  =  removerStringP1 (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | powerUp mapa (c+1) l  = removerStringP (explodirR mapa (c+1) l (r-1)) (c+1) l
                        | otherwise = explodirR mapa (c+1) l (r-1)




--------------------------------------------------------------------------------------------------------------------------------------------
--AUXILIARES PARA AS BOMBAS E PARA AS OUTRAS STRINGS DE POWERUPS E PLAYERS
-- Testa se há Tijolo
brick :: Map -> Coluna -> Linha -> Bool
brick mapa c l = (mapa !! l) !! c == '?'

--Testa se ha pedra
rock :: Map -> Coluna -> Linha -> Bool
rock mapa c l = (mapa !! l) !! c == '#'

--------Dá a coluna de uma string, tanto de uma bomba como de powerUp e player
colunaP :: String -> Coluna
colunaP l = read (takeWhile (/=' ') (drop 2 l))::Int

--------Dá a linha de uma string
linhaP :: String -> Linha
linhaP l = read (takeWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 2 l))))::Int

-------Dá o raio da string de uma bomba
raioB :: Bomba -> Raio
raioB l = read (takeWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 1 (dropWhile (/=' ') (drop 2 l))))))))::Int

-------- Dá o tempo que uma bomba ainda tem até explodir
tempo :: Bomba -> Tempo
tempo bomba = read (last (words bomba))::Int


jog :: Bomba -> Int 
jog l = read ((words l) !! 3)::Int
--------------------------------Auxiliares para as explosoes
-------Remove o '?' do mapa :
remover :: Map -> Coluna -> Linha -> Map
remover [] _ _ = []
remover (x:xs) col lin = case lin of
                          0 -> (remover1 x col) : xs
                          lin -> x : remover xs col (lin-1)

remover1 :: String -> Coluna -> String
remover1 [] _ = []
remover1 (x:xs) col = case col of
                        0 -> ' ' : xs
                        col -> x : remover1 xs (col-1)



-------Diz se há um powerUp naquela determinada posiçao:
powerUp :: Map -> Coluna -> Linha -> Bool
powerUp [] col lin = False
powerUp (x:xs) col lin | '+' == head x && col == colunaP x && lin == linhaP x = True
                       | '!' == head x && col == colunaP x && lin == linhaP x = True
                       | otherwise = powerUp xs col lin

-------Dada uma string diz se é o power up que queremos
power :: String -> Coluna -> Linha -> Bool
power [] _ _ = False
power (x:xs) col lin | x == '+' && col == colunaP (x:xs) && lin == linhaP (x:xs) = True
                     | x == '!' && col == colunaP (x:xs) && lin == linhaP (x:xs) = True
                     | otherwise = False

-------Diz se há um jogador naquela determinada posição:
player :: Map -> Coluna -> Linha -> Bool
player [] col lin = False
player (x:xs) col lin = if (serPlayer x) && col == colunaP x && lin == linhaP x then True else player xs col lin

--Diz se é um jogador:
serPlayer :: String -> Bool
serPlayer [] = False
serPlayer (x:xs) = if x == '0' || x == '1' || x == '2' || x == '3' then       True         else       False

-------Diz se há uma bomba naquela determinada posição:
bomb :: Map -> Coluna -> Linha -> Bool
bomb [] _ _ = False
bomb (x:xs) col lin | head x == '*' && col == colunaP x && lin == linhaP x = True
                    | otherwise = bomb xs col lin

-------Quando detetar uma bomba, esta função diminui o tempo que resta para explodir para 1 para explodir logo de seguida:
mudarTempo :: Map -> Coluna -> Linha  -> Map
mudarTempo [] _ _ = []
mudarTempo (x:xs) col lin | head x =='*' && col == colunaP x && lin == linhaP x = mudarTempo2 x 1 : xs
                          | otherwise = x : mudarTempo xs col lin

-------- Esta funçao muda o tempo das bombas para o tempo que quisermos
mudarTempo2 :: String -> Tempo -> String
mudarTempo2 bomba t = unwords ["*", show (colunaP bomba) , show (linhaP bomba), show (jog bomba), show (raioB bomba) , show t ]


-------Remove a String de um powerUp no caso de ser destruido pela bomba
removerStringP :: Map -> Coluna -> Linha  -> Map
removerStringP [] _ _ = []
removerStringP (x:xs) col lin   | power x col lin && colunaP x == col && linhaP x == lin = removerStringP xs col lin
                                | otherwise = x : removerStringP xs col lin

--------Remove a string de um jogador
removerStringP1 :: Map -> Coluna -> Linha  -> Map
removerStringP1 [] _ _ = []
removerStringP1 (x:xs) col lin   | serPlayer x && colunaP x == col && linhaP x == lin = removerStringP1 xs col lin
                                 | otherwise = x : removerStringP1 xs col lin

--------Esta funçao remove uma bomba do Map
remBomba :: Map -> Coluna -> Linha -> Map
remBomba [] _ _ = []
remBomba (x:xs) col lin | (head x) == '*' && (colunaP x == col) && (linhaP x) == lin = xs
                        | otherwise = x : remBomba xs col lin

----------No sitio onde é colocada a bomba , destroi o que estiver la
raioZero :: Map -> Coluna -> Linha -> Map
raioZero mapa c l | player mapa c l = removerStringP1 mapa c l
                  | powerUp mapa c l = removerStringP mapa c l
                  | otherwise = mapa
------------------------------------------------------------------------ESPIRAL----------------------------------------------------------------
--Funcao que da as coordenadas sem repeticoes dos sitios para pos as pedras
ultimateEspiral :: Map -> Coluna -> Linha -> [(Int,Int)]
ultimateEspiral l t c = nub (espiral l t 1)

---- Funçao que da as coordenadas das pedras com alguns repetidos
espiral :: Map -> Coluna -> Linha -> [(Int,Int)]
espiral (x:xs) time c | c <= div (length x) 3 = ((invocaR (x:xs) c c q)++(invocaD (x:xs) q c q)++(invocaL (x:xs) w q w)++(invocaU (x:xs) c w e))++(espiral (x:xs) time (c+1))
                      | otherwise = [(c,c)]
                      where
                        q = ((length x) -(c+1))
                        w = ((length x) -(c+2))
                        e = ((length x) -(c+3))

--Quando as pedras caminham para a direita , baixo, esquerda e cima
invocaR :: Map -> Coluna -> Linha -> Int -> [(Int,Int)]
invocaR (x:xs) c l i = if i /= 0 then (c,l) : invocaR (x:xs) (c+1) l (i-1)           else []

invocaD :: Map -> Coluna -> Linha -> Int -> [(Int,Int)]
invocaD (x:xs) c l i = if i /= 0 then (c,l) : invocaD (x:xs) c (l+1) (i-1)           else []

invocaL :: Map -> Coluna -> Linha -> Int -> [(Int,Int)]
invocaL (x:xs) c l i = if i /= 0 then (c,l) : invocaL (x:xs) (c-1) l (i-1)           else []

invocaU :: Map -> Coluna -> Linha  -> Int -> [(Int,Int)]
invocaU (x:xs) c l i = if i /= 0 then (c,l) : invocaU (x:xs) c (l-1) (i-1)           else []

----------Funcao que vai destruir os players e tal quando levam com uma pedra , e poe as pedras nas posicoes
espiral2 :: Map -> Coluna -> Linha -> Map
espiral2 mapa col lin | player mapa col lin             = (removerStringP1 (add mapa (col,lin)) col lin )
                      | powerUp mapa col lin            = removerStringP (add mapa (col,lin)) col lin
                      | (mapa !! lin) !! col == '#'     = mapa
                      | bomb mapa col lin               = remBomba (add mapa (col,lin)) col lin
                      | otherwise                       = add mapa (col,lin)

--Substitui as coordenadas por pedras no mapa nas posicoes
substitui :: Map -> Coordenadas -> Map
substitui mapa (c,l) = (espiral2 mapa c l)

-------Adiciona um '#' numa posicao
add :: Map -> Coordenadas -> Map
add (x:xs) (col,lin) = case lin of
                            0 ->  (add1 x col) : xs
                            _ ->  x : add xs (col,(lin-1))

add1 :: String -> Coluna -> String
add1 (x:xs) col = case col of
                    0 -> '#' : xs
                    _ -> x : add1 xs (col-1)
