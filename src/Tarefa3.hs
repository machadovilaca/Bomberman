{-|
 Module         : @Main@

 Descrição      : Reação a loMovimentos para o jogo Bomberman

 Autores        :

 * João Pedro Machado Vilaça, a82339@alunos.uminho.pt

 * João Nuno Alves Lopes, a80397@alunos.uminho.pt

 Acompanhamento : <http://di.uminho.pt/ Departamento de Informática da UMinho>

 O módulo recebe um estado do jogo, que se destina a ser guardado, e comprimi-o,
 de modo a ocupar o mínimo de espaço possível.

 Neste módulo é, ainda, possível descomprimir um estado de jogo, previamente guardado
 de modo a que o jogo possa ser retomado no estado em que foi pausado.
-}
module Main (main) where
import System.IO
import System.Environment
import Data.List.Split
import Data.Char

{-|

=Encode

O Programa recebe um estado de jogo e comprimi-o:

==Exemplos de utilização:

>>> encode ["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","! 5 5","* 7 7 1 1 10","0 4 3 +","1 6 6"]
"9-H2C1B1A1B1A1B1C2C2C.+5 2.+3 3.! 5 5.*7 7 1 1 10.0 4 3 +.1 6 6"


=Decode

O programa recebe um estado de jogo, previamente comprimido, e devolve-o ao seu estado original:

==Exemplos de utilização:

>>> decode "9-H2C1B1A1B1A1B1C2C2C.+5 2.+3 3.!5 5.*7 7 1 1 10.0 4 3 +.1 6 6"
["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","! 5 5","* 7 7 1 1 10","0 4 3 +","1 6 6"]





-}
main :: IO ()
main = do a <- getArgs
          let p = a !! 0
          w <- getContents
          if length a == 1 && length p == 2 && (p=="-e" || p=="-d")
             then if p=="-e" then putStr $ encode $ lines w
                             else putStr $ unlines $ decode w
             else putStrLn "Parâmetros inválidos"



--------------------------------------------------------------------------------------ENCODE------------------------------------------------------------------------------------


encode :: [String] -> String
encode l = arroba  (retiint(separado (separar l)))

arroba :: String -> String
arroba [] = []
arroba (h:t) = if h == '@'
               then arroba t
               else h:arroba t


retiint :: String -> String
retiint "" = ""
retiint (x:xs) | x == '?' = show (length (takeWhile (=='?') (x:xs))) ++ retiint (drop (length (takeWhile (=='?') (x:xs))) (x:xs))
               | otherwise = x : retiint xs


aux1 :: String -> String
aux1 (x:xs) = takeWhile (=='?') (x:xs)

retiraEspaço :: String -> String
retiraEspaço "" = ""
retiraEspaço [x,y] = [x,y]
retiraEspaço (x:y:xs) | x == '+' || x == '!' || x == '*' = x : retiraEspaço xs
                      | x == '.' && y == '0' = (x:y:xs)
                      | otherwise = x : retiraEspaço (y:xs)


encode1 :: [String] -> String
encode1 [] = []
encode1 l = (mais (ponto l))

separado :: [String] -> String
separado [] = []
separado (h:t) = (((chr ((fst (brancos h))+64))):[]) ++ (snd (brancos h)) ++ separado t

brancos :: String -> (Int,String)
brancos l = (  length (takeWhile (== ' ') l)   ,   drop (length (takeWhile (== ' ') l)) l      )

separar :: [String] -> [String]
separar l = splitOn "," (virgulas (encode1 l))

virgulas :: String -> String
virgulas [] = []
virgulas [x] = [x]
virgulas (a:b:t) | b == '.' = a:',':b:t
                 | a /= ' ' && b /= ' ' = a:       virgulas (b:t)
                 | a /= ' ' && b == ' ' = a:',':b :virgulas t
                 | a == ' ' && b == ' ' = a: virgulas (b:t)
                 | otherwise = a: virgulas (b:t)


ponto :: [String] -> [String]
ponto [] = []
ponto (x:xs) = if head x == '+' || head x == '!'  || head x == '*'  || head x == '0' || head x == '1' || head x == '2'  || head x == '3'
               then ("."++ x) : ponto xs
               else        x  : ponto xs

mais :: [String] -> String
mais l = (show (length (head l)) ++ "-" ++ retiraParede1 l)


retiraParede1 :: [String] -> String
retiraParede1 [] = []
retiraParede1 (h:t) = retiraParede h ++ retiraParede1 t

retiraParede :: String -> String
retiraParede [] = []
retiraParede (x:xs) = if x == '#'
                      then retiraParede xs
                      else x: retiraParede xs






-- ["#########","#       #","# # # # #","#   ??  #","# #?# # #","#? ?  ? #","#?# # #?#","# ?   ??#","#########","+ 5 2+ 3 3! 5 5* 7 7 1 1 100 4 3 +1 6 6"]





-----------------------------------------------------------------------------------DECODE---------------------------------------------------------------------------------------


decode :: String -> [String]
decode l = verifica (porEspaco (decode2 (decode3 ( decode4 l))))

verifica :: [String] -> [String]
verifica [] = []
verifica (h:t) = if h == ""
                 then verifica t
                 else h:verifica t

decode4 :: String -> String
decode4 [] = []
decode4 [x] = [x]
decode4 [x,z] = [x,z]
decode4 (a:b:c:t) | b == '-' = a:b:decode4 (c:t)
                  | c == '-' = a:b:c:decode4 t
                  | algarismos a && algarismos b = (replicate (stringToInt (a:b:[])) '?') ++ decode4 (c:t)
                  | algarismos a = (replicate (stringToInt (a:[])) '?') ++ decode4 (b:c:t)
                  | a == '.' = (a:b:c:t)
                  | otherwise = a:decode4 (b:c:t)

-- decode4 :: [String] -> [String]
-- decode4 [] = []
-- decode4 (x:xs) = (auxiliarB x) : decode4 xs


algarismos :: Char -> Bool
algarismos x | x >= '0' && x <= '9' = True
             | otherwise = False

stringToInt :: String -> Int
stringToInt [] = 0
stringToInt (x:xs) | length (x:xs) == 1 = (digitToInt x)
                   | length (x:xs) == 2 = ((digitToInt x) * 10) + (digitToInt  (head xs))
                   | otherwise = 0

charToString :: Char -> String
charToString c = [c]


auxiliar :: String -> String
auxiliar (x:xs) = x : auxiliarB xs


auxiliarB :: String -> String
auxiliarB "" = ""
auxiliarB (h:t) | (algarismos h) && (algarismos (head t)) = (replicate (stringToInt (h:(head t):"")) '?') ++ (auxiliarB (drop 2 (h:t)))
                | (algarismos h) = (replicate (stringToInt (h:"")) '?') ++ (auxiliarB (drop 1 (h:t)))
                | otherwise = h : (auxiliarB t)


porEspaco :: [String] -> [String]
porEspaco [] = []
porEspaco (x:xs) = (porAux x) : porEspaco xs

porAux :: String -> String
porAux "" = ""
porAux (x:xs) | x == '+' || x == '!' || x == '*' = x : xs
              | otherwise = (x:xs)


decode3 :: String -> String
decode3 [] = []
decode3 (h:t) | h == '@' = decode3 t
              | ord h > 64 = (replicate ((ord h)-64) ' ') ++ (decode3 t)
              | otherwise = h:decode3 t

decode2 :: String -> [String]
decode2 l = (decode1 l ++ (splitOn "." (resto l)))





resto :: String -> String
resto [] = []
resto (h:t) = if h == '+' || h == '!'  || h == '*'  || h == '0'
              then (h:t)
              else resto t

decode1 :: String -> [String]
decode1 [] = []
decode1 (a:b:c:t) | b == '-' = substitui1 (mostrarMapa (read (a:[]) :: Int)) (c:t)
                  | c == '-' = substitui1 (mostrarMapa (read (a:b:[]) :: Int)) t


substitui1 :: [String] -> String -> [String]
substitui1 [] l = []
substitui1 (x:xs) l = substitui x l : (substitui1 xs (drop (espacosX x) l))

espacosX :: String -> Int
espacosX [] = 0
espacosX (h:t) = if h == ' ' || h == 'r' then 1 + espacosX t else espacosX t

substitui :: String -> String -> String
substitui l [] = l
substitui [] l = []
substitui (x:xs) (y:ys) = if x == ' ' || x == 'r'
                          then (y : substitui xs    ys )
                          else (x : substitui xs (y:ys))





ex = ["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","! 5 5","* 7 7 1 1 10","0 4 3 +","1 6 6"]




mostrarMapa n = mostrarMapa' n n

mostrarMapa' :: Int -> Int -> [[Char]]
--mostrarMapa' vai utilizar a função linha para definir por recursividade todas as linhas do mapa

mostrarMapa' 1 b = [(linha 1 b b)]
mostrarMapa' n b = (linha n b b):(mostrarMapa' (n-1) b)



linha :: Int -> Int -> Int -> [Char]
--a funçao linha devolve a linha pedida em mostrarMapa' de acordo com o tamanho total do mapa

linha a b c   | a == 1 && c == 1          = ['#']
              | a == 1                    = '#' : linha a b (c-1)
--forma da ultima linha



--forma da penultima linha
              | a == 2 && c == 1          = ['#']
              | a == 2 && c == b          = '#' : linha a b (c-1)
              | a == 2 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == 2 && c + 2 == b      = ' ' : linha a b (c-1)
              | a == 2 && c == 2          = ' ' : linha a b (c-1)
              | a == 2 && c == 3          = ' ' : linha a b (c-1)
              | a == 2                    = ' ' : linha a b (c-1)




              | a == 3 && c == 1          = ['#']
              | a == 3 && c == b          = '#' : linha a b (c-1)
              | a == 3 && c == 2          = ' ' : linha a b (c-1)
              | a == 3 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == 3 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | a == 3 && (mod c 2) == 0  = ' ' : linha a b (c-1)
--forma da antepenultima linha



--forma da 1a linha
              | a == b && c == 1          = ['#']
              | a == b                    = '#' : linha a b (c-1)




--forma da 2a linha

              | a == b-1 && c == 1        = ['#']
              | a == b-1 && c == b        = '#' : linha a b (c-1)
              | a == b-1 && c + 1 == b    = ' ' : linha a b (c-1)
              | a == b-1 && c + 2 == b    = ' ' : linha a b (c-1)
              | a == b-1 && c == 2        = ' ' : linha a b (c-1)
              | a == b-1 && c == 3        = ' ' : linha a b (c-1)
              | a == b-1                  = ' ' : linha a b (c-1)



--forma da 3a linha
              | a == b-2 && c == 1          = ['#']
              | a == b-2 && c == b          = '#' : linha a b (c-1)
              | a == b-2 && c == 2          = ' ' : linha a b (c-1)
              | a == b-2 && c + 1 == b      = ' ' : linha a b (c-1)
              | a == b-2 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | a == b-2 && (mod c 2) == 0  = ' ' : linha a b (c-1)





--formação das restantes linhas restantes linhas
              | (mod a 2)==0 &&  c == 1           = ['#']
              | (mod a 2)==0 &&  c == b           = '#' : linha a b (c-1)
              | (mod a 2)==0                      = ' ' : linha a b (c-1)

              | (mod a 2) /= 0 &&  c == 1         = ['#']
              | (mod a 2) /= 0 &&  c == b         = '#' : linha a b (c-1)
              | (mod a 2) /= 0 && (mod c 2) /= 0  = '#' : linha a b (c-1)
              | (mod a 2) /= 0 && (mod c 2) == 0  = ' ' : linha a b (c-1)
