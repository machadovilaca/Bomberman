{-|
Module         : @Main@

Descrição      : Bomberman

Autores        :   João Pedro Machado Vilaça, a82339@alunos.uminho.pt e João Nuno Alves Lopes, a80397@alunos.uminho.pt

Acompanhamento : <http://di.uminho.pt/ Departamento de Informática da UMinho>

Este módulo contém definições de /Haskell/ (particularmente da
biblioteca __Gloss__) que forma uma interface gráfica para o jogo
Bomberman, de acordo com várias indicações do utilizador.
-}
module Main (main,
             tamanho,
             tempoJogo,
             seed,
             players,
             comandos,
             converte,
             junta,
             desconverte,
             estadoInicial,
             desenhaEstado,
             reageEvento,
             reageTempo,
             fr,
             dm,) where
import Graphics.Gloss
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Interface.Pure.Game
import Data.Char
import Control.Exception.Base
import Ajuda1
import Ajuda2
import Ajuda4
import Ajuda6


type Column = Int
type Line = Int
type Jogador = Int
type Range = Int
type Time = Int
type Powerups = String

-- | Tipo de dados para os PowerUps. Cujo 1º elemento corresponde ao identificador "+" ou "!"
type InfoPowerups = (String, Column, Linha)                 {-posição dos powerups-}

-- | Tipo de dados para as bombas.
type InfoBomba    = (String, Column, Line, Jogador, Range, Time)  {-posição e caraterísticas das bombas-}

-- | Tipo de dados para os jogadores.
type InfoJogador  = (Jogador, Column, Line , Powerups)           {-posição e powerups dos jogadores-}

type Mapa = [String]


-- | Uma representação do estado do jogo.
type Estado = (
               Mapa       {-mapa propriamente dito-}
              ,[InfoPowerups]
              ,[InfoBomba]
              ,[InfoJogador]
              ,[Picture]      {-bitmap's-}
              ,(Float,Int)
              )

-- | Uma representação do estado do jogo sem as imagens bitmap
type Estado1 = (
               Mapa        {-mapa propriamente dito-}
              ,[InfoPowerups]
              ,[InfoBomba]
              ,[InfoJogador]
              )




main :: IO ()
{-| O Programa pede ao utilizador para definir tamanho do mapa, o tempo de jogo, uma semente geradora do mapa e ainda o número de utilizadores

= Utilização do Executável:

== No terminal:

>>>./Tarefa5
Digite um tamanho, necessariamente ímpar, para o mapa:
13
Tempo de Jogo:
3-Longo, 2-Médio, 1-Curto
2
Digite um número gerador para o formato do mapa:
14
Digite o número de jogadores (1-4):
4
Comandos jogador 1 -> KeyUp, KeyDown, KeyLeft, KeyRight, P
Comandos jogador 2 ->   W  ,    D   ,    A   ,    D    , B
Comandos jogador 3 ->   T  ,    G   ,    F   ,    H    , M
Comandos jogador 4 ->   I  ,    K   ,    J   ,    L    , V
Pressione 'Enter' para continuar!
-}
main = do pedra      <- loadBMP "images/pedra.bmp"
          destruir   <- loadBMP "images/destruir.bmp"
          mario      <- loadBMP "images/mario.bmp"
          luigi      <- loadBMP "images/luigi.bmp"
          wario      <- loadBMP "images/wario.bmp"
          waluigi    <- loadBMP "images/waluigi.bmp"
          bomba      <- loadBMP "images/bomb.bmp"
          bombas     <- loadBMP "images/bombas.bmp"
          flames     <- loadBMP "images/flames.bmp"
          logo       <- loadBMP "images/logo.bmp"
          miei       <- loadBMP "images/miei.bmp"
          miei1      <- loadBMP "images/miei1.bmp"
          j1         <- loadBMP "images/j1.bmp"
          j2         <- loadBMP "images/j2.bmp"
          j3         <- loadBMP "images/j3.bmp"
          j4         <- loadBMP "images/j4.bmp"
          t1         <- loadBMP "images/t1.bmp"
          t2         <- loadBMP "images/t2.bmp"
          bflame     <- loadBMP "images/bflame.bmp"
          f1         <- loadBMP "images/f1.bmp"
          f2         <- loadBMP "images/f2.bmp"
          z <- tamanho
          i <- tempoJogo z
          x <- seed
          y <- players
          a <- comandos y
          play dm                     -- display mode
               (greyN 1)              -- côr do fundo da janela
               fr                     -- frame rate
               (estadoInicial (i,y) [z, x, y] [pedra, destruir, mario, luigi, wario, waluigi, bomba, bombas, flames, logo, miei, miei1, j1, j2, j3, j4, t1, t2, bflame, f1, f2])  -- estado inicial
               desenhaEstado          -- desenha o estado do jogo
               reageEvento            -- reage a um evento
               reageTempo             -- reage ao passar do tempo


-- | Usa o tamanho de mapa definido pelos utilizadores.
tamanho :: IO Int
tamanho = do putStrLn ("Digite um tamanho, necessariamente ímpar, para o mapa:")
             l <- getLine
             if erro l then tamanhoaux l else do putStrLn "Parâmetros Inválidos"
                                                 putStrLn ""
                                                 tamanho


tamanhoaux :: String -> IO Int
tamanhoaux l = do let x = read l :: Int
                  if odd x then do putStrLn ""
                                   return x
                  else do putStrLn "Parâmetros Inválidos"
                          putStrLn ""
                          tamanho


-- | Pede aos utilizadores qual a duração de tempo de jogo que querem. Longo, Médio ou Curto.
tempoJogo :: Int -> IO Float
tempoJogo n = do putStrLn ("Tempo de Jogo:")
                 putStrLn ("3-Longo, 2-Médio, 1-Curto")
                 l <- getLine
                 if l=="1" || l=="2" || l=="3"  then tempoaux l n
                 else do putStrLn "Parâmetros Inválidos"
                         putStrLn ""
                         tempoJogo n

tempoaux :: String -> Int -> IO Float

tempoaux l n = do let x = read l :: Int
                  let y = (x * n * 10)
                  let z = (read (((takeWhile (/='.') (show y)))) :: Float)
                  putStrLn ""
                  return z


-- | Usa o gerador aleatório definido pelo utilizador para formar o mapa.
seed :: IO Int
seed = do putStrLn ("Digite um número gerador para o formato do mapa:")
          l <- getLine
          if erro l then seedaux l else do putStrLn "Parâmetros Inválidos"
                                           putStrLn ""
                                           seed

seedaux :: String -> IO Int

seedaux l = do let x = read l :: Int
               putStrLn ""
               return x



-- | Recebe o número de utilizadores que vão usar o programa. Caso seja 1, o bot é ativado
players :: IO Int
players = do putStrLn ("Digite o número de jogadores (1-4):")
             l <- getLine
             if erro l then playersaux l else do putStrLn "Parâmetros Inválidos"
                                                 putStrLn ""
                                                 players

playersaux :: String -> IO Int

playersaux l = do let x = read l :: Int
                  if x<5 && x>0 then do putStrLn ""
                                        return x
                  else do putStrLn "Parâmetros Inválidos"
                          putStrLn ""
                          players



-- | Informa os utilizadores dos comandos para o jogo.
comandos :: Int -> IO Int
comandos y | y == 1 = do putStrLn ("Comandos jogador 1 -> KeyUp, KeyDown, KeyLeft, KeyRight, P")
                         putStrLn ("Pressione 'Enter' para continuar!")
                         l <- getLine
                         return 1

           | y == 2 = do putStrLn ("Comandos jogador 1 -> KeyUp, KeyDown, KeyLeft, KeyRight, P")
                         putStrLn ("Comandos jogador 2 ->   W  ,    D   ,    A   ,    D    , B")
                         putStrLn ("Pressione 'Enter' para continuar!")
                         l <- getLine
                         return 1

           | y == 3 = do putStrLn ("Comandos jogador 1 -> KeyUp, KeyDown, KeyLeft, KeyRight, P")
                         putStrLn ("Comandos jogador 2 ->   W  ,    D   ,    A   ,    D    , B")
                         putStrLn ("Comandos jogador 3 ->   T  ,    G   ,    F   ,    H    , M")
                         putStrLn ("Pressione 'Enter' para continuar!")
                         l <- getLine
                         return 1

           | y == 4 = do putStrLn ("Comandos jogador 1 -> KeyUp, KeyDown, KeyLeft, KeyRight, P")
                         putStrLn ("Comandos jogador 2 ->   W  ,    D   ,    A   ,    D    , B")
                         putStrLn ("Comandos jogador 3 ->   T  ,    G   ,    F   ,    H    , M")
                         putStrLn ("Comandos jogador 4 ->   I  ,    K   ,    J   ,    L    , V")
                         putStrLn ("Pressione 'Enter' para continuar!")
                         l <- getLine
                         return 1





erro :: String -> Bool

erro [] = True
erro (h:t) = if isDigit h then erro t else False




-- | Transforma o tipo de dados original (passo 1) para a estrutura melhorada
converte :: Mapa -> Estado1
converte a = converte1 a ([],[],[],[])

converte1 :: Mapa -> Estado1 -> Estado1

-- converte ["##","  ","##","+ 1 1","* 1 2","0 1 3"] - (["##","  ","##"],["+ 1 1"],["* 1 2 0 1 10"],["0 1 3"])
converte1 []    st                                         = st
converte1 (h:t) (a,b,c,d) | head h == '#'                  = converte1 t ( a++[h] ,   b ,   c    ,   d ) -- mapa para 1o elemento
                          | head h == '+' || head h == '!' = converte1 t (   a    , b++[i] ,   c ,   d ) -- powerups para o 2o e.
                          | head h == '*'                  = converte1 t (   a    ,   b , c++[j] ,   d ) -- bombas para 3o
                          | otherwise                      = converte1 t (   a    ,   b ,   c    , d++[k] ) -- jogadores 4o

            where i = ( (!!) (words h) 0 , stringToInt ((!!) (words h) 1) , stringToInt ((!!) (words h) 2) )

                  j = ( (!!) (words h) 0 , stringToInt ((!!) (words h) 1) , stringToInt ((!!) (words h) 2) ,
                        stringToInt ((!!) (words h) 3) , stringToInt ((!!) (words h) 4) , stringToInt ((!!) (words h) 5) )

                  k = if length (words h) == 4
                      then ( stringToInt ((!!) (words h) 0) , stringToInt ((!!) (words h) 1) ,
                             stringToInt ((!!) (words h) 2) , (!!) (words h) 3 )
                      else ( stringToInt ((!!) (words h) 0) , stringToInt ((!!) (words h) 1) ,
                             stringToInt ((!!) (words h) 2) , [] )



-- | Adiciona os bitmap's, resultando no tipo de dados principal (passo 2)
junta :: Estado1 -> [Picture] -> (Float,Int) -> Estado
junta (a,b,c,d) p i = (a,b,c,d,p,i)


-- | Função que desfaz o efeito das duas anteriores (para utilizar funções das tarefas anteriores)
desconverte :: Estado1 -> Mapa
desconverte (a, b, c, d) = a ++ dpowerups b ++ dbombas c ++ djogadores d

dpowerups :: [(String,Int,Int)] -> [String]

dpowerups [] = []
dpowerups ((b1,b2,b3):t) = (b1 ++ " " ++ show b2 ++ " " ++ show b3) : dpowerups t

dbombas :: [(String,Int,Int,Int,Int,Int)] -> [String]

dbombas [] = []
dbombas ((b1,b2,b3,b4,b5,b6):t) = (b1 ++ " " ++ show b2 ++ " " ++ show b3 ++ " " ++ show b4 ++ " " ++ show b5 ++ " " ++ show b6) : dbombas t

djogadores :: [InfoJogador] -> [String]

djogadores [] = []
djogadores ((b1,b2,b3,b4):t) = (show b1 ++ " " ++ show b2 ++ " " ++ show b3 ++ " " ++ b4) : djogadores t










-- | O estado inicial do jogo.
estadoInicial :: (Float,Int) -- ^ Tempo de Jogo e Indicador de Utilização de Bot (sim se = 1 )
              -> [Int]       -- ^ Informações para criação do mapa (tamanho, seed e número de jogadores)
              -> [Picture]   -- ^ Lista de BMP's
              -> Estado      -- ^ Resultado a ser usado
estadoInicial m [a, n, i] z
                      = junta (estadoInicial2 a n i) z m





estadoInicial2 :: Int -> Int -> Int -> Estado1

estadoInicial2 a n i = estadoInicial1 a n i



estadoInicial1 :: Int -> Int -> Int -> Estado1

-- possibilidade de ser o jogador a escolher dimensão e seed do mapa
-- quantidade de jogadores
estadoInicial1 x y z = converte (usarMapa x y z)



-- | Mapa gerado na Tarefa 1 ao qual é adicionada a posição original de cada jogador.
usarMapa :: Int -> Int -> Int -> Mapa
usarMapa x y z = case z of
               1 -> (mapa x y) ++ [ "0 1 1" ,
                                   ( "1" ++ " " ++ (show (x-2)) ++ " " ++ (show(x-2)))]

               2 -> (mapa x y) ++ [ "0 1 1" ,
                                   ( "1" ++ " " ++ (show (x-2)) ++ " " ++ (show(x-2)))]

               3 -> (mapa x y) ++ [ "0 1 1" ,
                                   ( "1" ++ " " ++ (show (x-2)) ++ " " ++ "1"),
                                   ( "2" ++ " " ++ "1" ++ " " ++ (show (x-2)))]

               4 -> (mapa x y) ++ [ "0 1 1" ,
                                   ( "1" ++ " " ++ (show (x-2)) ++ " " ++ "1"),
                                   ( "2" ++ " " ++ "1" ++ " " ++ (show (x-2))),
                                   ( "3" ++ " " ++ (show (x-2)) ++ " " ++ (show (x-2)))]









-- | Função que desenha o jogo.
desenhaEstado :: Estado  -- ^ Recebe as informações do jogo
              -> Picture -- ^ Produz o ambiente gráfico

desenhaEstado (_,_,_,[],[x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21],_)
                                                           =scale 2.25 2.25 $ translate 0 (-120) $ pictures (
                                                              [(translate (-160) 50  (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (-140) 100 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (-140) 200 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (-150) 150 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (0) 40  (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (-10) 100 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (110) 200 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))]
                                                            ++[(translate (40) 150 (Color black ( scale 0.25 0.25 (Text ("LOSERS")))))])


desenhaEstado (_,_,_,[(a, b, c , d)],[x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21],_)
                                                            = scale 2.25 2.25 $ translate 0 (-120) $ pictures (
                                                                 [(translate 0 160 ( scale 0.5 0.5 x20 ))]
                                                              ++ [(translate 0 165 ( scale 0.15 0.15 x21 ))]
                                                              ++ [(translate (-160) 40 (Color black ( scale 0.25 0.25 (Text ("Jogador "++ show (a+1) ++ " Ganhou!")))))])

desenhaEstado (a,b,c,d,[x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21],(i,j))
              = scale 2.25 2.25 ( translate 120 (-120) (pictures
                                ([base]
                                ++ (jajuda d 180 (x13,(scale 0.05 0.05 x3)) (x14,(scale 0.01 0.01 x4)) (x15,(scale 0.04 0.04 x5)) (x16,(scale 0.05 0.05 x6)))
                                ++ (jpowerups d 90 180 x8 x9)
                                ++ [logo,miei,miei1]
                                ++ (legenda1 ++ legenda2)
                                ++ (powerupss b tamanho x8 x9)
                                ++ (quadrados a 0 tamanho 0 x1 x2)
                                ++ (bombar c tamanho x7)
                                ++ (jogadores d tamanho x3 x4 x5 x6 )
                                ++ (bombflame a c tamanho x19)
                                ++ (legenda3)
                                )))

              where base     = color (dark1 green) (polygon [(-230,5),(-5,5), (-5,230),(-230,230)])
                    tamanho  = length (head a)
                    logo     = translate (-320) 180 (scale 0.3 0.3 x10)
                    miei     = translate (-320) 100 (scale 0.2 0.2 x11)
                    miei1    = translate (-320) 70  (scale 0.2 0.2 x12)
                    legenda1 = [translate 35 50 (scale 0.01 0.01 x8) , translate 85 50 (scale 0.1 0.1 x17)]
                    legenda2 = [translate 35 25 (scale 0.03 0.03 x9) , translate 90 25 (scale 0.1 0.1 x18)]
                    legenda3 = [translate (35) 200 (Color black ( scale 0.25 0.25 (Text ((takeWhile (/='.') (show i)))  )  ) )     ]



bombflame :: [String] -> [InfoBomba] -> Int -> Picture -> [Picture]

bombflame a b k i  = imagens (newMap (bombflameaux b) a) 0 k 0 i

imagens :: [String] -> Int -> Int -> Int -> Picture -> [Picture]

imagens [] _ _ _ _                      = []
imagens ([]:as) n i h y                 = imagens as 0 i (h+1) y
imagens ((x:xs):as) n i h y | x == '$'  = translate
                                            (trans (-227 + (n*(compri i)))) (trans (227 - (h*(compri i)))) (scale ((0.03*(13))/(j)) ((0.03*(13))/(j)) y)
                                            : (imagens ((xs):as) (n+1) i h y)

                            | otherwise = (imagens ((xs):as) (n+1) i h y)
                where j = read ((takeWhile (/='.') (show i))) :: Float


newMap :: [(Int,Int)] -> Mapa -> Mapa

newMap []     x = x
newMap (a:as) x = newMap as (newMap1 a x)

newMap1 :: (Int,Int) -> Mapa -> Mapa

newMap1 (a,b) s = x ++ [g++z] ++ ys
           where (x,(y:ys))   = splitAt b s
                 (g,(h:hs))   = splitAt a y
                 z            = if h == '#' then h:hs else '$':hs

bombflameaux :: [ InfoBomba ] -> [( Int,Int )]

bombflameaux [] = []
bombflameaux ((_,n2,n3,_,n5,n6):t) = if n6 == 1
                                     then [(n2,n3)] ++ tU (n2,n3,n5) ++ tD (n2,n3,n5) ++ tL (n2,n3,n5) ++ tR (n2,n3,n5) ++ bombflameaux t
                                     else bombflameaux t

tU :: (Int,Int,Int) -> [(Int,Int)]

tU (a,b,c) | c == 0    = []
           | otherwise = (a,b+1) : tU (a,b+1,c-1)

tD :: (Int,Int,Int) -> [(Int,Int)]

tD (a,b,c) | c == 0    = []
           | otherwise = (a,b-1) : tD (a,b-1,c-1)

tL :: (Int,Int,Int) -> [(Int,Int)]

tL (a,b,c) | c == 0    = []
           | otherwise = (a-1,b) : tL (a+1,b,c-1)

tR :: (Int,Int,Int) -> [(Int,Int)]

tR (a,b,c) | c == 0    = []
           | otherwise = (a+1,b) : tR (a+1,b,c-1)






jpowerups :: [InfoJogador] -> Float -> Float -> Picture -> Picture -> [Picture]

jpowerups [] _ _ _ _                             = []
jpowerups ((_,_,_,[]):t) i n x y                 = jpowerups t 90 (n-30) x y
jpowerups ((a,b,c,d) :t) i n x y | head d == '+' = translate i n (scale 0.01 0.01 x) : jpowerups  ((a,b,c,(tail d)):t) (i+20) n x y
                                 | head d == '!' = translate i n (scale 0.03 0.03 y) : jpowerups  ((a,b,c,(tail d)):t) (i+20) n x y


jajuda :: [InfoJogador] -> Float -> (Picture,Picture) -> (Picture,Picture)-> (Picture,Picture)-> (Picture,Picture) -> [Picture]

jajuda [] _ _ _ _ _ = []
jajuda ((a,b,c,d):t) n (x1,y1) (x2,y2) (x3,y3) (x4,y4) | a == 0 =  translate 35 n (scale 0.15 0.15 x1) :
                                                                   (translate 75 n y1) : jajuda t (n-30) (x1,y1) (x2,y2) (x3,y3) (x4,y4)
                                                       | a == 1 =  translate 35 n (scale 0.15 0.15 x2) :
                                                                   (translate 75 n y2) : jajuda t (n-30) (x1,y1) (x2,y2) (x3,y3) (x4,y4)
                                                       | a == 2 =  translate 35 n (scale 0.15 0.15 x3) :
                                                                   (translate 75 n y3) : jajuda t (n-30) (x1,y1) (x2,y2) (x3,y3) (x4,y4)
                                                       | a == 3 =  translate 35 n (scale 0.15 0.15 x4) :
                                                                   (translate 75 n y4) : jajuda t (n-30) (x1,y1) (x2,y2) (x3,y3) (x4,y4)

jogadores :: [(Int, Int, Int, String)] -> Int -> Picture -> Picture -> Picture -> Picture -> [Picture]

jogadores [] _ _ _ _ _= []
jogadores ((a,b,c,d):t) i x y z z2 | a == 0 = translate
                                                      (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.05*13)/j) ((0.05*13)/j) x)
                                                      : (jogadores t i x y z z2)
                                   | a == 1 = translate
                                                      (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.01*13)/j) ((0.01*13)/j) y)
                                                      : (jogadores t i x y z z2)
                                   | a == 2 = translate
                                                      (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.04*13)/j) ((0.04*13)/j) z)
                                                      : (jogadores t i x y z z2)
                                   | a == 3 = translate
                                                      (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.05*13)/j) ((0.05*13)/j) z2)
                                                      : (jogadores t i x y z z2)
                                where j = read ((takeWhile (/='.') (show i))) :: Float


quadrados :: [String] -> Int -> Int -> Int-> Picture -> Picture -> [Picture]

quadrados [] _ _ _ pedra destruir = []

quadrados ([]:as) n i h pedra destruir                 = quadrados as 0 i (h+1) pedra destruir

quadrados ((x:xs):as) n i h pedra destruir | x == '#'  = translate
                                                                 (trans (-227 + (n*(compri i)))) (trans (227 - (h*(compri i)))) (scale ((0.1*13)/j) ((0.1*13)/j) pedra)
                                                                 : (quadrados ((xs):as) (n+1) i h pedra destruir)

                                           | x == '?'  = translate
                                                                 (trans (-227 + (n*(compri i)))) (trans (227 - (h*(compri i)))) (scale ((0.05*13)/j) ((0.05*13)/j) destruir)
                                                                 : (quadrados ((xs):as) (n+1) i h pedra destruir)

                                           | otherwise = (quadrados ((xs):as) (n+1) i h pedra destruir)
                where j = read ((takeWhile (/='.') (show i))) :: Float


bombar :: [InfoBomba] -> Int -> Picture -> [Picture]

bombar [] _ _ = []
bombar ((a,b,c,d,e,f):t) i x = translate
                                        (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.03*13)/j) ((0.03*13)/j) x)
                                        : (bombar t i x)
                       where j = read ((takeWhile (/='.') (show i))) :: Float

powerupss :: [InfoPowerups] -> Int -> Picture -> Picture -> [Picture]

powerupss [] _ _ _ = []
powerupss ((a,b,c):t) i x y | a == "+" = translate
                                        (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.01*13)/j) ((0.01*13)/j) x)
                                        : (powerupss t i x y)

                            | a == "!" = translate
                                        (trans (-227 + (b*(compri i)))) (trans (227 - (c*(compri i)))) (scale ((0.03*13)/j) ((0.03*13)/j) y)
                                        : (powerupss t i x y)
                       where j = read ((takeWhile (/='.') (show i))) :: Float


compri :: Int -> Int

compri 0 = 0
compri x = div 240 x

trans :: Int -> Float

trans a = read ((takeWhile (/='.') (show a))) :: Float

-- Darken a color, adding black, definido nas bibliotecas do haskell
dark1 :: Color -> Color

dark1 c
 = let  (r, g, b, a)    = rgbaOfColor c
   in   makeColor (r - 0.5) (g - 0.5) (b - 0.5) a















-- | Função que altera o estado do jogo quando acontece um evento.
reageEvento :: Event   -- ^ Com a receção de um comando
            -> Estado  -- ^ Sobre estado atual efetua-se o consequente desse evento
            -> Estado  -- ^ Podendo ou não resultar num novo Estado.

-- keys e p pertencem ao jogador 0
reageEvento _ (x1,x2,x3,[(a, b, c , d)],x4,x5) = (x1,x2,x3,[(a, b, c , d)],x4,x5)
reageEvento (EventKey (SpecialKey KeyUp)    Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 0 'U' )) e i
reageEvento (EventKey (SpecialKey KeyLeft)  Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 0 'L' )) e i
reageEvento (EventKey (SpecialKey KeyDown)  Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 0 'D' )) e i
reageEvento (EventKey (SpecialKey KeyRight) Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 0 'R' )) e i
reageEvento (EventKey (Char 'p')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 0 'B' )) e i
reageEvento (EventKey (Char 'w')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 1 'U' )) e i
reageEvento (EventKey (Char 's')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 1 'D' )) e i
reageEvento (EventKey (Char 'd')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 1 'R' )) e i
reageEvento (EventKey (Char 'a')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 1 'L' )) e i
reageEvento (EventKey (Char 'b')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 1 'B' )) e i
reageEvento (EventKey (Char 't')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 2 'U' )) e i
reageEvento (EventKey (Char 'g')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 2 'D' )) e i
reageEvento (EventKey (Char 'f')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 2 'L' )) e i
reageEvento (EventKey (Char 'h')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 2 'R' )) e i
reageEvento (EventKey (Char 'm')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 2 'B' )) e i
reageEvento (EventKey (Char 'i')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 3 'U' )) e i
reageEvento (EventKey (Char 'k')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 3 'D' )) e i
reageEvento (EventKey (Char 'j')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 3 'L' )) e i
reageEvento (EventKey (Char 'l')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 3 'R' )) e i
reageEvento (EventKey (Char 'v')            Down _ _ ) (a,b,c,d,e,i)  =  junta (converte (move (desconverte (a,b,c,d)) 3 'B' )) e i
reageEvento _ e = e




-- | Função que altera o estado do jogo quando o tempo avança @x@ segundos.
reageTempo :: Float   -- ^ Após alguns instantes é atualizado o espaço do tempo
           -> Estado  -- ^ Todos os elementos dependentes de um momento de tempo sofrem alterações
           -> Estado  -- ^ Podendo ou não resultar num novo Estado.

reageTempo _ (x1,x2,x3,[(a, b, c , d)],x4,x5) = (x1,x2,x3,[(a, b, c , d)],x4,x5)
reageTempo x (a,b,c,d,e,(i,k)) | k==1 = case bot (desconverte (a,b,c,d)) 1 m of
                                        Just 'U' -> junta (converte (move (avanca (desconverte (a,b,c,d)) m) 1 'U' )) e (i-0.15,k)
                                        Just 'R' -> junta (converte (move (avanca (desconverte (a,b,c,d)) m) 1 'R' )) e (i-0.15,k)
                                        Just 'L' -> junta (converte (move (avanca (desconverte (a,b,c,d)) m) 1 'L' )) e (i-0.15,k)
                                        Just 'D' -> junta (converte (move (avanca (desconverte (a,b,c,d)) m) 1 'D' )) e (i-0.15,k)
                                        Just 'B' -> junta (converte (move (avanca (desconverte (a,b,c,d)) m) 1 'B' )) e (i-0.15,k)
                                        Nothing ->  junta (converte (avanca (desconverte (a,b,c,d)) m) ) e ((i - 0.15),k)
                               | otherwise = junta (converte (avanca (desconverte (a,b,c,d)) m) ) e ((i - 0.15),k)
                      where z = takeWhile (/='.') (show x)
                            y = read z :: Int
                            l = takeWhile (/='.') (show i)
                            m = read l :: Int




-- | Frame rate.
fr :: Int
fr = 8


-- | Display mode.
dm :: Display
dm = InWindow "Bomberman, Get Ready to Die!" (1200,600) (0, 0)
