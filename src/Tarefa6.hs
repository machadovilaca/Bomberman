{-|
Module         : @Main@

Descrição      : Bomberman

Autores        :   João Pedro Machado Vilaça, a82339@alunos.uminho.pt e João Nuno Alves Lopes, a80397@alunos.uminho.pt

Acompanhamento : <http://di.uminho.pt/ Departamento de Informática da UMinho>

Bot que se movimenta em função do estado de jogo independentemente de um utilizador humano.
-}
module Main (main,
             inDanger,
             bot,
             foge,
             centra) where

import Data.Char
import Data.Maybe
import Text.Read
import System.Environment
import System.Random

type Map = [String]
type Mapa = [String]
type Bomba = String
type Coluna = Int
type Linha = Int
type Coordenadas = (Coluna,Linha)
type Raio = Int
type Position = (Int,Int)

ex =  ["#########","#       #","# #?#?# #","#  ?  ? #","#?# # #?#","# ?  ?  #","# #?#?# #","#  ??   #","#########","+ 5 2","+ 3 3","! 5 5","* 7 7 1 1 0","0 4 3 +","1 6 6"]

main :: IO ()
{-| Faz um conjunto de movimentos dependendo do estado de jogo para tentar fugir a bombas colocadas pelos outros jogadores e tentando
colocar bombas para se movimentar no mapa e para tentar ser o único sobrevivente.
Tenta escapar à espiral colocando-se no centro do mapa
-}
main = do a <- getArgs
          let player = readMaybe (a !! 0)
          let ticks = readMaybe (a !! 1)
          w <- getContents
          if isJust player && isJust ticks then putStr $ show $ bot (lines w) (fromJust player) (fromJust ticks) else putStrLn "Parâmetros inválidos"




-- | Função principal que decide qual a ação do bot
bot :: Mapa -- ^ Recebe as informações do jogo
    -> Int -- ^ Indicação de identidade do nosso jogador
    -> Int -- ^ Tempo para o final do Jogo
    -> Maybe Char -- ^ Movimento a efetuar, Up, Down, Left Right or Nothing

bot mapa player _ = if inDanger mapa jog || bombaMinha mapa player then foge mapa jog
                                                                   else centra mapa jog

            where jog1 = filter (\x -> head x == (intToDigit player)) mapa
                  jog2 = words (head jog1)
                  jog  = (read (jog2 !! 1) :: Int , read (jog2 !! 2) :: Int)

----------------
bombaMinha :: Mapa -> Int -> Bool
bombaMinha mapa i = tem (filter (\x -> head x == '*') mapa) i

tem :: [String] -> Int -> Bool
tem [] _ = False
tem (h:t) i = or (boolTem (h:t) i)

boolTem :: [String] -> Int -> [Bool]
boolTem [] _ = []
boolTem (h:t) i = ((words h !! 3) == (show i)) : boolTem t i
 ---------------------------------------------------------------------------------------------

-- | Verifica a situação atual do jogador ou se efetuando um movimento para um local ficará em perigo.
inDanger :: Mapa  -- ^ No Estado Atual
         -> Position -- ^ Nesta posição está ou estaria em perigo?
         -> Bool     -- ^ Terá de se desviar? Sim ou Não

inDanger mapa (a,b) = deBombas bomb (a,b)
           where bomb = map words (filter (\x -> (head x == '*')) mapa)


deBombas :: [Map] -> Position -> Bool
deBombas [] _ = False
deBombas (h:t) (a,b) | h !! 1 == show a || h !! 2 == show b = True
                    | testeBombas (c, l) r (a,b) = True
                    | otherwise = deBombas t (a,b)

                    where c = read (h !! 1) :: Int
                          l = read (h !! 2) :: Int
                          r = (read (h !! 4) :: Int) +1


testeBombas :: Position -> Int -> Position -> Bool
testeBombas (c,l) 0 (x,y) = c == x || l == y
testeBombas (c,l) r (x,y) | c == x && l+r == y = True
                         | c == x && l-r == y = True
                         | c-r == x && l == y = True
                         | c+r == x && l == y = True
                         | otherwise = testeBombas (c,l) (r-1) (x,y)


-------------------------------------------------------------------------------------

-- | Em caso de perigo de explosão de bomba move o jogador
foge :: Mapa  -- ^ Recebe as informações do jogo
     -> Position  -- ^ A posição do jogador
     -> Maybe Char -- ^ Ordena um movimento

foge mapa (a,b)
               | can mapa (a,b+1) && can mapa (a,b+2)   && (not (inDanger mapa (a,b+1))) = Just 'D'
               | can mapa (a,b+1) && can mapa (a+1,b+1) && (not (inDanger mapa (a,b+1))) = Just 'D'
               | can mapa (a,b+1) && can mapa (a-1,b+1) && (not (inDanger mapa (a,b+1))) = Just 'D'

               | can mapa (a-1,b) && can mapa (a-2,b)   && (not (inDanger mapa (a-1,b))) = Just 'L'
               | can mapa (a-1,b) && can mapa (a-1,b+1) && (not (inDanger mapa (a-1,b))) = Just 'L'
               | can mapa (a-1,b) && can mapa (a-1,b-1) && (not (inDanger mapa (a-1,b))) = Just 'L'

               | can mapa (a,b-1) && can mapa (a,b-2)   && (not (inDanger mapa (a,b-1))) = Just 'U'
               | can mapa (a,b-1) && can mapa (a+1,b-1) && (not (inDanger mapa (a,b-1))) = Just 'U'
               | can mapa (a,b-1) && can mapa (a-1,b-1) && (not (inDanger mapa (a,b-1))) = Just 'U'

               | can mapa (a+1,b) && can mapa (a+2,b)   && (not (inDanger mapa (a+1,b))) = Just 'R'
               | can mapa (a+1,b) && can mapa (a+1,b+1) && (not (inDanger mapa (a+1,b))) = Just 'R'
               | can mapa (a+1,b) && can mapa (a+1,b-1) && (not (inDanger mapa (a+1,b))) = Just 'R'

               | can mapa (a,b+1) && can mapa (a,b+2) && can mapa (a,b+3) && bombaDele mapa (a,b) =  Just 'D'
               | can mapa (a+1,b) && can mapa (a+2,b) && can mapa (a+3,b) && bombaDele mapa (a,b) =  Just 'R'
               | can mapa (a,b-1) && can mapa (a,b-2) && can mapa (a,b-3) && bombaDele mapa (a,b) =  Just 'U'
               | can mapa (a-1,b) && can mapa (a-2,b) && can mapa (a-3,b) && bombaDele mapa (a,b) =  Just 'L'

               | can mapa (a,b+1) && can mapa (a,b+2) && bombaDele mapa (a,b) =  Just 'D'
               | can mapa (a+1,b) && can mapa (a+2,b) && bombaDele mapa (a,b) =  Just 'R'
               | can mapa (a,b-1) && can mapa (a,b-2) && bombaDele mapa (a,b) =  Just 'U'
               | can mapa (a-1,b) && can mapa (a-2,b) && bombaDele mapa (a,b) =  Just 'L'

               | can mapa (a,b+1) && bombaDele mapa (a,b) =  Just 'D'
               | can mapa (a+1,b) && bombaDele mapa (a,b) =  Just 'R'
               | can mapa (a,b-1) && bombaDele mapa (a,b) =  Just 'U'
               | can mapa (a-1,b) && bombaDele mapa (a,b) =  Just 'L'

               | can mapa (a,b+1) && can mapa (a,b+2) && can mapa (a,b+3) && (not (inDanger mapa (a,b+1))) = Just 'D'
               | can mapa (a+1,b) && can mapa (a+2,b) && can mapa (a+3,b) && (not (inDanger mapa (a+1,b))) = Just 'R'
               | can mapa (a,b-1) && can mapa (a,b-2) && can mapa (a,b-3) && (not (inDanger mapa (a,b-1))) = Just 'U'
               | can mapa (a-1,b) && can mapa (a-2,b) && can mapa (a-3,b) && (not (inDanger mapa (a-1,b))) = Just 'L'

               | can mapa (a,b+1) && can mapa (a,b+2) && (not (inDanger mapa (a,b+1))) = Just 'D'
               | can mapa (a+1,b) && can mapa (a+2,b) && (not (inDanger mapa (a+1,b))) = Just 'R'
               | can mapa (a,b-1) && can mapa (a,b-2) && (not (inDanger mapa (a,b-1))) = Just 'U'
               | can mapa (a-1,b) && can mapa (a-2,b) && (not (inDanger mapa (a-1,b))) = Just 'L'

               | can mapa (a,b+1) && (not (inDanger mapa (a,b+1))) = Just 'D'
               | can mapa (a+1,b) && (not (inDanger mapa (a+1,b))) = Just 'R'
               | can mapa (a,b-1) && (not (inDanger mapa (a,b-1))) = Just 'U'
               | can mapa (a-1,b) && (not (inDanger mapa (a-1,b))) = Just 'L'


               | otherwise = Nothing


-- | Em caso de segurança ou aproximação do fim, centra-se no mapa desviando-se da espiral
centra :: Mapa       -- ^ Recebe as informações do jogo
       -> Position   -- ^ Posição do jogador
       -> Maybe Char -- ^ Indica direção do centro

centra mapa (a,b) | a == meio1 && b == meio+1 = Nothing
                 | b < meio && can mapa (a,b+1) && can mapa (a,b+2) && can mapa (a,b+3) && (not (inDanger mapa (a,b+1))) = Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+2,b) && can mapa (a+3,b) && (not (inDanger mapa (a+1,b)))= Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a,b-2) && can mapa (a,b-3) && (not (inDanger mapa (a,b-1))) = Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-2,b) && can mapa (a-3,b) && (not (inDanger mapa (a-1,b))) = Just 'L'

                 | b < meio && can mapa (a,b+1) && can mapa (a,b+2) && (not (inDanger mapa (a,b+1)))= Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+2,b) && (not (inDanger mapa (a+1,b)))= Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a,b-2) && (not (inDanger mapa (a,b-1)))= Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-2,b) && (not (inDanger mapa (a-1,b)))= Just 'L'

                 | b < meio && can mapa (a,b+1) && can mapa (a+1,b+1) && (not (inDanger mapa (a,b+1)))= Just 'D'
                 | b < meio && can mapa (a,b+1) && can mapa (a-1,b+1) && (not (inDanger mapa (a,b+1)))= Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+1,b+1) && (not (inDanger mapa (a+1,b)))= Just 'R'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+1,b-1) && (not (inDanger mapa (a+1,b)))= Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a+1,b-1) && (not (inDanger mapa (a,b-1)))= Just 'U'
                 | b > meio && can mapa (a,b-1) && can mapa (a-1,b-1) && (not (inDanger mapa (a,b-1)))= Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-1,b+1) && (not (inDanger mapa (a-1,b)))= Just 'L'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-1,b-1) && (not (inDanger mapa (a-1,b)))= Just 'L'

                 | b < meio && can mapa (a,b+1) && (not (inDanger mapa (a,b+1)))= Just 'D'
                 | a < meio1 && can mapa (a+1,b) && (not (inDanger mapa (a+1,b)))= Just 'R'
                 | b > meio && can mapa (a,b-1) && (not (inDanger mapa (a,b-1)))= Just 'U'
                 | a > meio1 && can mapa (a-1,b) && (not (inDanger mapa (a-1,b)))= Just 'L'

                 | b < meio && can mapa (a,b+1) && can mapa (a,b+2) && can mapa (a,b+3) = Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+2,b) && can mapa (a+3,b) = Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a,b-2) && can mapa (a,b-3) = Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-2,b) && can mapa (a-3,b) = Just 'L'

                 | b < meio && can mapa (a,b+1) && can mapa (a,b+2) = Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+2,b) = Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a,b-2) = Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-2,b) = Just 'L'

                 | b < meio && can mapa (a,b+1) && can mapa (a+1,b+1) = Just 'D'
                 | b < meio && can mapa (a,b+1) && can mapa (a-1,b+1) = Just 'D'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+1,b+1) = Just 'R'
                 | a < meio1 && can mapa (a+1,b) && can mapa (a+1,b-1) = Just 'R'
                 | b > meio && can mapa (a,b-1) && can mapa (a+1,b-1) = Just 'U'
                 | b > meio && can mapa (a,b-1) && can mapa (a-1,b-1) = Just 'U'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-1,b+1) = Just 'L'
                 | a > meio1 && can mapa (a-1,b) && can mapa (a-1,b-1) = Just 'L'

                 | b < meio && can mapa (a,b+1) = Just 'D'
                 | a < meio1 && can mapa (a+1,b) = Just 'R'
                 | b > meio && can mapa (a,b-1) = Just 'U'
                 | a > meio1 && can mapa (a-1,b) = Just 'L'

                 | otherwise = Just 'B'
         where meio  = (div (length (head mapa)-1) 2)
               meio1 = (div (length (head mapa)-1) 2) -1
-----------------------------------------------------------------------------------


posicao :: [[String]] -> [Position]
posicao [] = []
posicao (h:t) = (read (h !! 1) :: Int , read (h !! 2) :: Int) : posicao t


can :: Map -> Position -> Bool
can mapa (a,b) = ((mapa !! b) !! a) /= '#' && ((mapa !! b) !! a) /= '?' && noBomb mapa (a,b)

noBomb :: Map -> Position -> Bool
noBomb [] _ = True
noBomb mapa (a,b) = noBombs (filter (\x -> head x == '*') mapa) (a,b)

noBombs :: [String] -> Position -> Bool
noBombs [] _ = True
noBombs (h:t) (a,b) = and (noBombs1 (h:t) (a,b))

noBombs1 :: [String] -> Position -> [Bool]
noBombs1 [] _ = []
noBombs1 (h:t) (a,b) = ((read ((words h) !! 1) :: Int) /= a || (read ((words h) !! 2) :: Int) /= b) : noBombs1 t (a,b)

bombaDele :: Map -> Position -> Bool
bombaDele mapa (a,b) = minha10 (filter (\x -> head x == '*') mapa) (a,b)

minha10 :: [String] -> Position -> Bool
minha10 [] _ = False
minha10 (h:t) (a,b) = or (minha101 (h:t) (a,b))

minha101 :: [String] -> Position -> [Bool]
minha101 [] _ = []
minha101 (h:t) (a,b) = ((read ((words h) !! 1) :: Int) == a || (read ((words h) !! 2) :: Int) == b ) : minha101 t (a,b)
